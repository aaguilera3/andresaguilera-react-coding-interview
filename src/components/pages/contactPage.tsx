import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { contactsClient } from '@lib/clients/contacts';
import { IPerson } from '@lib/models/person';

const ContactPage = () => {
  const [contact, setContact] = useState<IPerson>(null);

  const fetchContact = async (pageSize: number, pageNumber: number) => {
      const list = await contactsClient.contactList({
          pageSize,
          pageNumber
      });
      setContact(list.data[0])
  };
  let { id } = useParams();
  console.log('id', id);

  useEffect(() => {
    fetchContact(1, 1);
  }, []);

  return <div>Hello 2</div>;
};

export default ContactPage;
