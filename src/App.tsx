import { RouterProvider, createBrowserRouter, Navigate, Outlet } from 'react-router-dom';

import { UIProvider } from '@contexts/uiContext';
import { routeDefinitions } from '@lib/routes';

import { MainLayout } from '@components/layouts';
import { ContactListPage } from '@components/pages';
import ContactPage from "@components/pages/contactPage";

const appRouter = createBrowserRouter([
  {
    path: '/',
    element: <MainLayout />,
    children: [
      {
        path: '/',
        element: <Navigate to="/contacts" />
      },
      {
        path: '/contacts',
        element: <Outlet />,
        children: [
          {
            path: '/contacts',
            element: <ContactListPage />,
            id: routeDefinitions.contactsList.id
          },
          {
            path: "edit/:id",
            element: <ContactPage />,
          },
        ]
      }
    ]
  }
]);

function App() {
  return (
    <UIProvider>
      <RouterProvider router={appRouter} />
    </UIProvider>
  );
}

export default App;
